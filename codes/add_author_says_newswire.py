#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
This module takes source, ere, and best files, adds 'The author says ' to the beginning
of each sentence in source, and update offsets, entity mentions and sources in ere and
best files. It produces new source, ere, and best files for input data.
'''
import sys
import re
import os
import random
import xml.etree.ElementTree as ET
from get_ranges_offsets import transform_source, get_src_ranges
from nltk import ne_chunk, pos_tag, word_tokenize, tokenize
from nltk.tree import Tree
from xml.etree.ElementTree import tostring
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from os import listdir, makedirs
from os.path import isfile, isdir, join, splitext, split, exists

__author__ = 'Tao Yu'
__email__ = 'ty2326@columbia.edu'
__date__ = 'June 15 2016'

def usage():
    print ("python add_author_says.py <input dir>")
    print ("<input dir> is the dir where contains source, ere, and annotation folders.")

def indent(elem, level=0):
    '''copy and paste from http://effbot.org/zone/element-lib.htm#prettyprint
    it basically walks your tree and adds spaces and newlines so the tree is
    printed in a nice way'''
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def to_orgid(mid):
    '''convert SINNET mid back to original mid if it was changed'''
    eid, bid = mid.split('-')
    change_padding = '999999'
    if bid.startswith(change_padding):
        ascii_nums = bid.replace(change_padding, '')
        n = 3 #each 3 digits represent a char
        chars = [chr(int(ascii_nums[i:i+n])) for i in range(0, len(ascii_nums), n)]
        org_bid = ''.join(c for c in chars)
        mid = '-'.join([eid, org_bid])

    return mid

def update_id(mid):
    '''convert mid into SINNET mid (with only numbers after '-')'''
    pattern_to_change = "(\w+)\-(\d+)$"
    change_padding = '999999'
    if not re.match(pattern_to_change, mid):
        try:
            eid, bid = mid.split('-')
            mid_chars = list(bid)
            digits_str = '-'.join([eid ,change_padding])
            for c in mid_chars:
                ascii = ord(c)
                ascii_str = str(ascii)
                if ascii < 100:
                    ascii_str = "0" + ascii_str
                digits_str += ascii_str
            mid = digits_str
        except:
            print '\n mid {0} in ere is not in right format!'.format(mid)

    return mid


def update_all_ids(root, ere_flag, to_flag):
    '''Update all ids in ere and best'''

    if ere_flag:
        elems_have_id = ['entity', 'entity_mention', 'filler', 'event_mention',\
                        'relation', 'relation_mention', 'hopper']
        elems_have_emfid = ['rel_arg1', 'rel_arg2', 'em_arg'] #entity_id, entity_mention_id, filler_id

        for elem_name in elems_have_id:
            for element in root.iter(elem_name):
                mid = element.get('id')
                new_id = update_id(mid)
                element.set('id', new_id)

        for elem_name in elems_have_emfid:
            for element in root.iter(elem_name):
                entity_id = element.get('entity_id')
                entity_mention_id = element.get('entity_mention_id')
                filler_id = element.get('filler_id')
                if not entity_id is None:
                    new_eid = update_id(entity_id)
                    element.set('entity_id', new_eid)
                if not entity_mention_id is None:
                    new_emid = update_id(entity_mention_id)
                    element.set('entity_mention_id', new_emid)
                if not filler_id is None:
                    new_fid = update_id(filler_id)
                    element.set('filler_id', new_fid)
    else:
        elems_have_ereid = ['relation', 'source', 'arg', 'entity', 'event']

        for elem_name in elems_have_ereid:
            for element in root.iter(elem_name):
                ereid = element.get('ere_id')
                if to_flag:
                    new_ereid = update_id(ereid)
                else:
                    new_ereid = to_orgid(ereid)
                element.set('ere_id', new_ereid)


def update_all_offset_docid(root, elems_have_offset, offset_change_ranges, flag, base_name_sinnet=None):
    '''
    iter over each element with offset attrib to update its offset (when flag is True)
    or set the offset back to the original one (when flag is False) based on offset_change_ranges
    '''
    for elem_name in elems_have_offset:
        for element in root.iter(elem_name):
            if base_name_sinnet != None: #update source id in ere files
                element.set('source', str(base_name_sinnet))

            if not element.get('offset') is None:
                old_offset = int(element.get('offset'))
                new_offset = old_offset
                for offset_added, offset_change_range in offset_change_ranges.items():
                    if flag: #update its offset (when flag is True)
                        left = offset_change_range[0]
                        right = offset_change_range[1]
                        if left <= old_offset < right: #check if the old offset in one of the ranges
                            new_offset = old_offset + offset_added #then add corresponding offset_movedback to get new offset
                            break
                    else: #set the offset back to the original one (when flag is Flase)
                        left = offset_change_range[0] + offset_added
                        right = offset_change_range[1] + offset_added
                        if left <= old_offset < right:
                            new_offset = old_offset - offset_added
                            break

                element.set('offset', str(new_offset))


def add_em_author(root, author_offsets, author_inds, ranges, base_name_sinnet):
    '''
    build author entity mentions for the author of a post or quote
    '''
    entities = root.find('entities')
    #add a new entity, and put all the author mentions under it
    auh_entid = 'ent-79532326'
    auh_type = 'PER'
    specificity="specific"
    author_entity = SubElement(entities, "entity", id=auh_entid, type=auh_type, specificity=specificity)

    for author_ind in author_inds: #for each offset of "The author says"
        id_num = random.randint(100000, 999999)
        em_id = 'm-' + str(id_num)
        noun_type = 'NAM'
        source = base_name_sinnet
        offset_added = str(author_ind)
        length = str(10)
        #if it is, then add it to em and its parent (entity) is the same as the real author
        entity_mention_added = SubElement(author_entity, "entity_mention", id=em_id,
                                            noun_type=noun_type, source=source,
                                            offset=offset_added, length=length)
        mention_text = SubElement(entity_mention_added, 'mention_text')
        mention_text.text = 'The author'


def transform_ere(ere_path, src_ranges, base_name_sinnet):
    '''
    Add new added 'The author' entity mentions to the entities of authors
    '''
    ere_tree = ET.parse(ere_path)
    ere_root = ere_tree.getroot()

    #update doc_id
    ere_root.set('doc_id', base_name_sinnet)

    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)

    post_ranges = src_ranges.post_ranges
    quote_ranges = src_ranges.quote_ranges
    ranges = post_ranges.copy()
    ranges.update(quote_ranges)

    author_inds = src_ranges.author_inds
    offset_change_ranges = src_ranges.offset_change_ranges

    #the elements whose attributes have offset
    ere_elems_have_offset = ['nom_head', 'entity_mention', 'filler', 'trigger']
    update_all_offset_docid(ere_root, ere_elems_have_offset, offset_change_ranges, True, base_name_sinnet)
    #start to add new added 'The author' entity_mention to ere
    add_em_author(ere_root, author_offsets, author_inds, ranges, base_name_sinnet)
    #check if id in ere should be updated
    update_all_ids(ere_root, True, True)

    return ere_root


def mid_offerset_dicts_getter(root):
    '''
    get dicts of entity mention to its offset and relation mention to its arg1's offset
    used to spot offset of targets of relations in best files
    '''
    offset_emid = {int(em.get('offset')) : to_orgid(em.get('id')) for em in root.iter('entity_mention')}
    emid_offset = {to_orgid(em.get('id')) : int(em.get('offset')) for em in root.iter('entity_mention')}
    emid_endoffset = {to_orgid(em.get('id')) : int(em.get('offset'))+ int(em.get('length')) for em in root.iter('entity_mention')} #int(em.get('offset') + em.get('length'))
    relm_offset = {to_orgid(relm.get('id')) : emid_offset[to_orgid(relm.find('rel_arg1').get('entity_mention_id'))] for relm in root.iter('relation_mention')}

    return (offset_emid, emid_offset, emid_endoffset, relm_offset)


def transform_pred_best_nw(best_root, ere_root, src_ranges):
    '''
    set src text in best.xml file back to original name of the poster or the quoter
    '''
    pred_best_root = best_root
    parent_map = {c:p for p in pred_best_root.iter() for c in p}

    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)

    post_ranges = src_ranges.post_ranges
    quote_ranges = src_ranges.quote_ranges
    ranges = post_ranges.copy()
    ranges.update(quote_ranges)

    offset_change_ranges = src_ranges.offset_change_ranges

    #get a dict of author and its attributes from ere file
    author_attrib = {}
    for entity_mention in ere_root.iter('entity_mention'):
        em_offset = int(entity_mention.get('offset'))
        if em_offset in author_offsets:
            author_id = entity_mention.get('id')
            author_length = entity_mention.get('length')
            mention_name = entity_mention.find('mention_text').text
            author_attrib[em_offset] = (author_id, author_length, mention_name)

    #find new added 'The author' sources, and then remove it
    for source in pred_best_root.iter('source'):
        if source.text == 'The author':
            p = parent_map[source]
            bs_attrib = p.attrib
            pp = parent_map[p]
            pp.remove(p)
            if pp.tag == 'beliefs':
                belief = SubElement(pp, "belief")
                belief.attrib = bs_attrib
            elif pp.tag == 'sentiments':
                sentiment = SubElement(pp, "sentiment")
                sentiment.attrib = bs_attrib

    #finally update all offset in the best file
    best_elems_have_offset = ['trigger', 'source', 'arg', 'entity']
    update_all_offset_docid(pred_best_root, best_elems_have_offset, offset_change_ranges, False)

    #check if id in ere should be updated
    update_all_ids(pred_best_root, False, False)

    return pred_best_root


def update_author_src(element, offset_emid, relm_offset, author_inds, author_offsets, is_belief):
    '''
    change the source that is an author of the element to 'the author' entity mention added above
    '''
    sources = []
    if element.tag == 'event':
        trigger = element.find('trigger')
        tag_offset = int(trigger.get('offset'))
    elif element.tag == 'relation':
        rel_id = element.get('ere_id')
        tag_offset = relm_offset[rel_id]
    elif element.tag == 'entity' or element.tag == 'arg':
        tag_offset = int(element.get('offset'))

    if is_belief:
        beliefs = element.find('beliefs')
        all_belief = beliefs.findall('belief')
        for belief in all_belief:
            source = belief.find('source')
            if source is None:
                #set attribs of the author source
                for ind in sorted(author_inds, reverse=True):
                    if tag_offset > ind and ind in offset_emid.keys(): #if set for <quote> ?
                        src_ereid = offset_emid[ind]
                        src_offset = str(ind)
                        src_len = str(10)
                        src_entity = SubElement(belief, "source", ere_id=src_ereid,
                                                offset=src_offset, length=src_len)
                        src_entity.text = 'The author'
                        break
    else:
        sentiments = element.find('sentiments')
        all_sentiment = sentiments.findall('sentiment')
        for sentiment in all_sentiment:
            source = sentiment.find('source')
            polarity = sentiment.get('polarity')
            if source is None and (polarity == 'pos' or polarity == 'neg'):
                #set attribs of the author source
                for ind in sorted(author_inds, reverse=True):
                    if tag_offset > ind and ind in offset_emid.keys(): #if set for <quote> ?
                        src_ereid = offset_emid[ind]
                        src_offset = str(ind)
                        src_len = str(10)
                        src_entity = SubElement(sentiment, "source", ere_id=src_ereid,
                                                offset=src_offset, length=src_len)
                        src_entity.text = 'The author'
                        break


def update_all_author_src(root, elems_have_source, offset_emid, relm_offset, author_inds, author_offsets):
    '''
    update all author entity mention for all element in the best file
    '''

    for elem_name in elems_have_source:
        if elem_name == 'entity':
            for elem in root.iter(elem_name):
                update_author_src(elem, offset_emid, relm_offset, author_inds, author_offsets, False)
        elif elem_name == 'arg':
            for elem in root.iter(elem_name):
                update_author_src(elem, offset_emid, relm_offset, author_inds, author_offsets, True)
        else:
            belief_annotations = root.find('belief_annotations')
            for elem_belief in belief_annotations.iter(elem_name):
                update_author_src(elem_belief, offset_emid, relm_offset, author_inds, author_offsets, True)
            sentiment_annotations = root.find('sentiment_annotations')
            for elem_sentiment in sentiment_annotations.iter(elem_name):
                update_author_src(elem_sentiment, offset_emid, relm_offset, author_inds, author_offsets, False)


def remove_none_polarity(best_root):
    # tes = best_root.find('sentiment_annotations')
    # best_root.remove(tes)
    parent_map = {c:p for p in best_root.iter() for c in p}
    elems_have_sentiments = ['entities', 'events', 'relations']
    sentiment_annotations = best_root.find('sentiment_annotations')
    for elem in elems_have_sentiments:
        ent_rel_event = sentiment_annotations.find(elem)
        # sentiment_annotations.remove(ent_rel_event)
        if ent_rel_event.tag == 'entities':
            for m in ent_rel_event.findall('entity'):
                for sentiment in m.iter('sentiment'):
                    polarity = sentiment.get('polarity')
                    if polarity == 'none':
                        ent_rel_event.remove(m)
                        break
        elif ent_rel_event.tag == 'events':
            for m in ent_rel_event.findall('event'):
                for sentiment in m.iter('sentiment'):
                    polarity = sentiment.get('polarity')
                    if polarity == 'none':
                        ent_rel_event.remove(m)
                        break
        elif ent_rel_event.tag == 'relations':
            for m in ent_rel_event.findall('relation'):
                for sentiment in m.iter('sentiment'):
                    polarity = sentiment.get('polarity')
                    if polarity == 'none':
                        ent_rel_event.remove(m)
                        break


def transform_best(best_path, src_ranges, ere_root):
    '''
    update author sources to new added 'The author' in the best file
    '''
    best_tree = ET.parse(best_path)
    best_root = best_tree.getroot()
    #get new 'the author' indexs in source and offset_change_ranges
    author_inds = src_ranges.author_inds
    offset_change_ranges = src_ranges.offset_change_ranges

    author_offsets = src_ranges.author_offsets
    quote_begins = src_ranges.quote_begins
    author_offsets.extend(quote_begins)

    #first update all offset in the best file
    best_elems_have_offset = ['trigger', 'source', 'arg', 'entity']
    update_all_offset_docid(best_root, best_elems_have_offset, offset_change_ranges, True)
    #get offset of targets of relations from the ere file
    offset_emid, emid_offset, emid_endoffset, relm_offset = mid_offerset_dicts_getter(ere_root)

    #call update_all_author_src for all element which has source to check
    elems_have_source = ['entity', 'arg', 'relation', 'event']
    update_all_author_src(best_root, elems_have_source, offset_emid, relm_offset, author_inds, author_offsets)
    #check if id in ere should be updated
    update_all_ids(best_root, False, True)
    #remove none polarity cases
    remove_none_polarity(best_root)

    return best_root


def transfrom_all_indir(input_dir):
    """
    process all source, ere, and annotation files in the input dir, and write all new source,
    ere, and annotation files to source_new, ere_new, and annotation_new folders in this dir.
    """
    if not isdir(input_dir):
        usage()
    else:
        src_input_dir = os.path.join(input_dir,"source")
        ere_input_dir = os.path.join(input_dir,"ere")
        best_input_dir = os.path.join(input_dir,"annotation")

        src_output_dir = os.path.join(input_dir,"source_original")
        ere_output_dir = os.path.join(input_dir,"ere_original")
        best_output_dir = os.path.join(input_dir,"annotation_original")
        predicted_best_dir = os.path.join(input_dir,"predicted_best")

        #if dirs do not exist, makedirs
        if not exists(src_output_dir):
            makedirs(src_output_dir)
        if not exists(ere_output_dir):
            makedirs(ere_output_dir)
        if not exists(best_output_dir):
            makedirs(best_output_dir)
        if not exists(predicted_best_dir):
            makedirs(predicted_best_dir)

        filenames = [f for f in os.listdir(src_input_dir) if f.endswith('.xml')]
        chars_to_remove = ['.', '_', '-', '-kbp']
        for f in filenames:
            base_name = f.replace('.xml','')
            print "processing file is: ", base_name
            ere_fn = '{0}.rich_ere.xml'.format(base_name)
            best_fn = '{0}.best.xml'.format(base_name)
            #input files
            src_path = os.path.join(src_input_dir, f)
            ere_path = os.path.join(ere_input_dir, ere_fn)
            best_path = os.path.join(best_input_dir, best_fn)

            #output files (change file names to SINNET input file names)
            base_name_sinnet = ''.join([c for c in base_name if c not in chars_to_remove])
            src_fn_sinnet = '{0}.cmp.txt'.format(base_name_sinnet)
            ere_fn_sinnet = '{0}.rich_ere.xml'.format(base_name_sinnet)
            best_fn_sinnet = '{0}.best.xml'.format(base_name_sinnet)
            pred_best_fn_sinnet = '{0}.best.xml'.format(base_name)
            src_new_path = os.path.join(src_output_dir, src_fn_sinnet)
            ere_new_path = os.path.join(ere_output_dir, ere_fn_sinnet)
            best_new_path = os.path.join(best_output_dir, best_fn_sinnet)
            predicted_best_path = os.path.join(predicted_best_dir, pred_best_fn_sinnet)

            #if ere and best files with the same name as source file exist
            if isfile(ere_path) and isfile(best_path):
                # src_old = get_src_ranges(src_path)
                #transform source files
                src_content = transform_source(src_path)
                #write new source file
                src_new_file = open(src_new_path, 'w')
                src_new_file.write(src_content.encode('utf8'))
                # src_new_file.write(src_content.encode('utf8'))
                src_new_file.close()

                #get new source ranges of authors, posts and quotes
                src_range_new = get_src_ranges(src_new_path)

                #transform the ere file
                ere_root = transform_ere(ere_path, src_range_new, base_name_sinnet)
                #write new ere file
                ere_new_file = open(ere_new_path, 'w')
                indent(ere_root)
                ere_new_file.write(ET.tostring(ere_root, encoding="UTF-8" ))
                ere_new_file.close()

                #transform the best file
                best_root = transform_best(best_path, src_range_new, ere_root)
                #write new best file
                best_new_file = open(best_new_path, 'w')
                indent(best_root)
                best_new_file.write(ET.tostring(best_root, encoding="UTF-8" ))
                best_new_file.close()

                #transform the new best files back to original best files as the final predicted best files
                pred_best_root = transform_pred_best(best_root, ere_root, src_range_new)
                #write predicted best file
                best_new_file = open(predicted_best_path, 'w')
                indent(pred_best_root)
                best_new_file.write(ET.tostring(pred_best_root, encoding="UTF-8" ))
                best_new_file.close()

        print('Successfully write new source, ere and annotation files to {0}.'.format(input_dir))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        transfrom_all_indir(sys.argv[1])
