#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
import pandas as pd
import json
import numpy as np
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from os.path import isfile, isdir, join

from to_pred_best import build_bestxml

def usage():
    print 'Usage: python build_baseline_eval.py [ere files dir] [baseline best files output dir]'

def mid_offerset_dicts_getter(root):
    '''
    get dicts of entity mention to its offset and relation mention to its arg1's offset
    used to spot offset of targets of relations in best files
    '''
    mid_offsets = {}
    emid_offset = {em.get('id') : int(em.get('offset')) for em in root.iter('entity_mention')}
    relm_offset = {relm.get('id') : emid_offset[relm.find('rel_arg1').get('entity_mention_id')] for relm in root.iter('relation_mention')}
    evm_offset = {evm.get('id') : int(evm.find('trigger').get('offset')) for evm in root.iter('event_mention')}
    mid_offsets.update(emid_offset)
    mid_offsets.update(relm_offset)
    mid_offsets.update(evm_offset)
    return mid_offsets


def get_sid(author_ranges_f, offset):
    sid = None
    for mid, ranges in author_ranges_f.items():
        for rg in ranges:
            if offset >= rg[0] and offset <= rg[1]:
                sid = mid
                break
        if sid != None:
            break

    return sid


def get_ptype(mention_name):
    ptype = 'entity'
    if mention_name == 'relation_mention':
        ptype = 'relation'
    elif mention_name == 'event_mention':
        ptype = 'event'

    return ptype

def build_baseline_file(base_name, ere_f, author_ranges_f):
    file_baseline = []
    ere_tree = ET.parse(ere_f)
    ere_root = ere_tree.getroot()
    mention_names = ['entity_mention', 'relation_mention', 'event_mention']
    mid_offsets = mid_offerset_dicts_getter(ere_root)

    for mention_name in mention_names:
        for mention in ere_root.iter(mention_name):
            tid = mention.get('id')
            target_offset = mid_offsets[tid]
            if target_offset != None:
                sid = get_sid(author_ranges_f, target_offset)
            else:
                print 'offset is none'
                sid = None

            ptype = get_ptype(mention_name)
            pred_belief = 'cb'
            pred_sentiment = 'neg'

            best_baseline = {'file_name': base_name, 'target_id': tid, 'source_id': sid,\
                            'parent_type': ptype, 'source_text': 'ignored', 'target_offset': target_offset,\
                            'source_offset': 'ingored', 'pred_belief': pred_belief, 'pred_sentiment': pred_sentiment}

            file_baseline.append(best_baseline)

    return file_baseline

def build_baseline(ere_dir, author_ranges, baseline_best_dir):
    """
    process all source, ere, and annotation files in the input dir, and write all new source,
    ere, and annotation files to source_new, ere_new, and annotation_new folders in this dir.
    """
    if not isdir(ere_dir):
        usage()
    else:
        total_baseline = []
        #change '.rich_ere.xml' to '.predicted.map.rich_ere.xml' for pred ere files
        filenames = [f for f in os.listdir(ere_dir) if f.endswith('.rich_ere.xml')]
        for f in filenames:
            base_name = f.replace('.rich_ere.xml', '')
            print 'Processing file: ', base_name
            try:
                author_ranges_f = author_ranges[base_name]
            except:
                print '{0} is not in author_ranges'.format(base_name)
                author_ranges_f = {}
            ere_f = os.path.join(ere_dir, f)
            if isfile(ere_f):
                file_baseline = build_baseline_file(base_name, ere_f, author_ranges_f)
                total_baseline.extend(file_baseline)

        print 'there are total {0} targets.\n'.format(len(total_baseline))

        baseline_df = pd.DataFrame(total_baseline)

        grouped_fname = baseline_df.groupby(['file_name'])
        sentiment_flag = True
        belief_flag = True
        pred_roots = build_bestxml(grouped_fname, sentiment_flag, belief_flag, baseline_best_dir)

        print('Successfully write the baseline best files to {0}.'.format(baseline_best_dir))


if __name__ == '__main__':
    if len(sys.argv) == 3:
        # read in author_ranges file
        author_ranges_f = '/Users/taoyds/Documents/BeSt/Data_DEFT/eval_predicted_ere/predicted_ere/spa/nw/author_ranges_predere_eval_spa_nw.json'
        with open(author_ranges_f) as data_file:
            author_ranges = json.load(data_file)

        ere_dir = sys.argv[1]
        baseline_output_dir = sys.argv[2]
        build_baseline(ere_dir, author_ranges, baseline_output_dir)

    else:
        print 'Usage: python build_baseline_eval.py [ere files dir] [baseline best files output dir]'
