'''
Tranform predictions in json file into best xml files so that Daniel's
evaluation scripts can use these produced files as predicted files in
the inputs.
'''

import json
import os
import sys
from os import listdir
from os.path import isfile, join
import pandas as pd
import xml.etree.ElementTree as ET
from xml.etree import ElementTree
from xml.etree.ElementTree import tostring
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement


def indent(elem, level=0):
    '''copy and paste from http://effbot.org/zone/element-lib.htm#prettyprint
    it basically walks your tree and adds spaces and newlines so the tree is
    printed in a nice way'''
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def xstr(s):
    converted = ''
    if s is None:
        converted = 'missed'
    else:
        try:
            converted = str(s)
        except:
            converted = 'ingored'
    return converted

def build_sentiments(parent, group_tid):
    '''
    Build the sentiment component for entity, event, or relation(parent).
    Here suppose each parent has only one sentiment
    '''
    sentiments_xml = SubElement(parent, 'sentiments')
    for index, g_tid in group_tid.iterrows():
        # polarity = g_tid['polarity'] #NEED CHANGE
        polarity = g_tid['pred_sentiment'] #NEED CHANGE
        sarcasm = 'ignored'
        ere_id = g_tid['source_id'] #NEED CHANGE
        offset = g_tid['source_offset'] #NEED CHANGE
        length = 'ignored'
        source_text = g_tid['source_text'] #NEED CHANGE

        sentiment_xml = SubElement(sentiments_xml, 'sentiment', polarity=xstr(polarity), sarcasm=xstr(sarcasm))
        if ere_id is not None and ere_id != 0:
            source = SubElement(sentiment_xml, 'source', ere_id=xstr(ere_id), offset=xstr(offset), length=xstr(length))
            # source.text = source_text


def build_beliefs(parent, group_tid):
    '''
    Build the beliefs component which may has several belief children for event, or relation(parent)
    '''
    beliefs_xml = SubElement(parent, 'beliefs')
    for index, g_tid in group_tid.iterrows():
        btype = g_tid['pred_belief']
        # btype = g_tid['pred_belief'] #NEED CHANGE
        polarity = 'ignored'
        sarcasm = 'ignored'
        ere_id = g_tid['source_id'] #NEED CHANGE
        offset = g_tid['source_offset'] #NEED CHANGE
        length = 'ignored'
        source_text = g_tid['source_text'] #NEED CHANGE

        #make SubElement object robust to any situation with a missing value
        belief_xml = SubElement(beliefs_xml, 'belief', type=xstr(btype), polarity=xstr(polarity), sarcasm=xstr(sarcasm))
        if ere_id is not None and ere_id != 0:
            source = SubElement(belief_xml, 'source', ere_id=xstr(ere_id), offset=xstr(offset), length=xstr(length))
            # source.text = xstr(source_text)


def build_relation(relations, group_tid, is_belief_annotations):
    '''
    Build relation elements for the relations component in belief_annotations or sentiment_annotations
    '''
    ere_id = group_tid['target_id'].iloc[0]
    relation = SubElement(relations, 'relation', ere_id=xstr(ere_id))
    trigger = SubElement(relation, 'trigger', offset='ignored')

    if is_belief_annotations:
        build_beliefs(relation, group_tid)
    else:
        build_sentiments(relation, group_tid)


def build_relations(annotations, group_ptype):
    '''
    Build relations elements for belief_annotations or sentiment_annotations
    '''
    relations = SubElement(annotations, 'relations')

    #groupby target id to build sentiments unit
    grouped_tid = group_ptype.groupby(['target_id'])

    for tid, group_tid in grouped_tid:
        if annotations.tag == 'belief_annotations':
            build_relation(relations, group_tid, True)
        elif annotations.tag == 'sentiment_annotations':
            build_relation(relations, group_tid, False)


def build_event(events, group_tid, is_belief_annotations):
    '''
    Build event elements for the events component in belief_annotations or sentiment_annotations
    '''
    ere_id = group_tid['target_id'].iloc[0]
    event = SubElement(events, 'event', ere_id=xstr(ere_id))
    trigger = SubElement(event, 'trigger', offset='ignored')

    if is_belief_annotations:
        build_beliefs(event, group_tid)
    else:
        build_sentiments(event, group_tid)


def build_events(annotations, group_ptype):
    '''
    Build the events element for belief_annotations or sentiment_annotations
    '''
    events = SubElement(annotations, 'events')

    #groupby target id to build sentiments unit
    grouped_tid = group_ptype.groupby(['target_id'])

    for tid, group_tid in grouped_tid:
        if annotations.tag == 'belief_annotations':
            build_event(events, group_tid, True)
        elif annotations.tag == 'sentiment_annotations':
            build_event(events, group_tid, False)


def build_entities(sentiment_annotations, group_ptype):
    '''
    Build the entities element for sentiment_annotations
    Notice: belief_annotations section does not have the entities element
    '''
    entities = SubElement(sentiment_annotations, 'entities')
    #groupby target id to build sentiments unit
    grouped_tid = group_ptype.groupby(['target_id'])
    for tid, group_tid in grouped_tid:
        ere_id = tid
        try:
            offset = group_tid['target_offset'].iloc[0]
        except:
            offset = 'ignored'
        length = 'ignored'
        #build entity unit
        entity = SubElement(entities, 'entity', ere_id=xstr(ere_id), offset=xstr(offset), length=xstr(length))
        try:
            entity_text = group_tid['target_text'].iloc[0]
        except:
            entity_text = 'ignored'
        text_xml = SubElement(entity, 'text')
        # text_xml.text = entity_text
        #build sentiments unit
        build_sentiments(entity, group_tid)


def build_bestxml(grouped, sentiment_flag, belief_flag, output_dir=None):
    '''
    Build and write deft_best.xml file from best_annotations object

    grouped : a pandas groupby dataframe

    '''
    #Here suppose it just has a single source
    pred_roots = {}
    sentiment_values = ['pos','neg']
    belief_values = ['cb','ncb','rob','na']
    for name, group in grouped:
        #build the root:committed_belief_doc
        belief_sentiment_doc = Element('belief_sentiment_doc')

        #build belief_annotations
        belief_annotations = SubElement(belief_sentiment_doc, 'belief_annotations')
        #build sentiment_annotations
        sentiment_annotations = SubElement(belief_sentiment_doc, 'sentiment_annotations')
        #groupby parent_type to create 'entity', 'relation', and 'event' sections
        grouped_ptype = group.groupby(['parent_type'])
        for ptype, group_ptype in grouped_ptype:
            #CHANGE: 'polarity'->'predicted polarity', 'belief_type'->'predicted belief_type'
            # polarity_preds = group_ptype[group_ptype['polarity'].isin(sentiment_values)]
            # belief_preds = group_ptype[group_ptype['belief_type'].isin(belief_values)]
            if sentiment_flag:
                polarity_preds = group_ptype[group_ptype['pred_sentiment'].isin(sentiment_values)]
                if ptype == 'entity':
                    build_entities(sentiment_annotations, polarity_preds)
                elif ptype == 'event':
                    build_events(sentiment_annotations, polarity_preds)
                elif ptype == 'relation':
                    build_relations(sentiment_annotations, polarity_preds)
                else:
                    raise AssertionError('Got unexpected mention type {0}.'.format(ptype))

            if belief_flag:
                belief_preds = group_ptype[group_ptype['pred_belief'].isin(belief_values)]
                if ptype == 'event':
                    build_events(belief_annotations, belief_preds)
                elif ptype == 'relation':
                    build_relations(belief_annotations, belief_preds)
                elif ptype == 'entity':
                    continue
                else:
                    raise AssertionError('Got unexpected mention type {0}.'.format(ptype))

        pred_roots[name] = belief_sentiment_doc
        #write to deft_best.xml file
        if output_dir != None:
            fname = name + '.best.xml'
            output_file = os.path.join(output_dir, fname)
            pred_file = open(output_file, 'w' )
            indent(belief_sentiment_doc)
            pred_file.write(ET.tostring(belief_sentiment_doc, encoding="UTF-8" ))
            pred_file.close()

    return pred_roots


def transform(test_pd, output_dir):
    grouped_fname = test_pd.groupby(['file_name'])
    sentiment_flag = True
    belief_flag = False
    pred_roots = build_bestxml(grouped_fname, sentiment_flag, belief_flag, output_dir)
    print('Successfully build predicted best files in {0}.'.format(output_dir))

    return pred_roots



if __name__ == '__main__':
    if len(sys.argv) == 3:
        test_pd = pd.read_json(sys.argv[1])
        print('Successfully read data from {0}.'.format(sys.argv[1]))
        grouped_fname = test_pd.groupby(['file_name'])
        #start to build best files
        output_dir = sys.argv[2]
        sentiment_flag = True
        belief_flag = False
        pred_roots = build_bestxml(grouped_fname, sentiment_flag, belief_flag, output_dir)
        print('Successfully build predicted best files in {0}.'.format(sys.argv[2]))
    else:
        print('to_pred_best.py -- library for converting preds into best xml files.')
        print(' Usage: python to_pred_best.py [test data file in json] [output dir]')
