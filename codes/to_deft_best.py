'''
This module contains methods that read the OOP representation of belief and
sentiment annotations into the best.xml file. In this way, it is easier to
compare predicted best.xml with gold best.xml.
'''

import sys
import xml.etree.ElementTree as ET
from xml.etree import ElementTree
from xml.etree.ElementTree import tostring
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from deft_ere import read_ere_xml
from deft_best import read_best_xml

import os
from os import listdir
from os.path import isfile, join

__author__ = 'Tao Yu'
__email__ = 'ty2326@columbia.edu'
__date__ = 'June 1 2016'



def xstr(s):
    if s is None:
        return ''
    else:
        return str(s)


def indent(elem, level=0):
    '''copy and paste from http://effbot.org/zone/element-lib.htm#prettyprint
    it basically walks your tree and adds spaces and newlines so the tree is
    printed in a nice way'''
    i = "\n" + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def build_beliefs(parent, beliefs):
    '''
    Build the beliefs component which may has several belief children for event, or relation(parent)
    '''
    beliefs_xml = SubElement(parent, 'beliefs')

    for belief in beliefs:
        btype = belief.belief_type
        polarity = belief.polarity
        sarcasm = belief.sarcasm
        #make SubElement object robust to any situation with a missing value
        belief_xml = SubElement(beliefs_xml, 'belief', type=xstr(btype), polarity=xstr(polarity), sarcasm=xstr(sarcasm))

        entity_mention = belief.source
        if entity_mention:
            ere_id = entity_mention.mention_id
            offset = entity_mention.offset
            length = entity_mention.length
            source_text = entity_mention.mention_text
            source = SubElement(belief_xml, 'source', ere_id=xstr(ere_id), offset=xstr(offset), length=xstr(length))
            source.text = source_text


def build_sentiments(parent, sentiments):
    '''
    Build the sentiment component for entity, event, or relation(parent).
    Here suppose each parent has only one sentiment
    '''

    # if we allow multiple sentiments toward a single entity. then do the following

    sentiments_xml = SubElement(parent, 'sentiments')

    for sentiment in sentiments:
        polarity = sentiment.polarity
        sarcasm = sentiment.sarcasm
        sentiment_xml = SubElement(sentiments_xml, 'sentiment', polarity=xstr(polarity), sarcasm=xstr(sarcasm))
        entity_mention = sentiment.source
        if entity_mention:
            ere_id = entity_mention.mention_id
            offset = entity_mention.offset
            length = entity_mention.length
            source_text = entity_mention.mention_text
            source = SubElement(sentiment_xml, 'source', ere_id=xstr(ere_id), offset=xstr(offset), length=xstr(length))
            source.text = source_text


def build_arguments(event, args):
    '''
    Build the arguments section for event element in belief_annotations part
    For the BeSt evaluation, arguments can be ignored
    Here suppose argument.beliefs are the same as event/beliefs (event_mention.beliefs)
    '''
    argments = SubElement(event, 'arguments')
    for arg in args:
        if arg.is_filler == True:
            entity_mention = arg.entity
            ere_id = entity_mention.filler_id
            text = entity_mention.text
        else:
            entity_mention = arg.entity_mention
            ere_id = entity_mention.mention_id
            text = entity_mention.mention_text

        offset = entity_mention.offset
        length = entity_mention.length
        arg_xml = SubElement(argments, 'arg', ere_id=xstr(ere_id), offset=xstr(offset), length=xstr(length))
        text_xml = SubElement(arg_xml, 'text')
        text_xml.text = text

        arg_beliefs = arg.beliefs
        if arg_beliefs:
            build_beliefs(arg_xml, arg_beliefs)


def build_trigger(parent, trigger):
    '''
    Build trigger section for event or relation elements
    '''
    offset = trigger.offset
    length = trigger.length
    trigger_text = trigger.text
    trigger_xml = SubElement(parent, 'trigger', offset=xstr(offset), length=xstr(length))
    trigger_xml.text = trigger_text


def build_relation(relations, relation_mention, is_belief_annotations):
    '''
    Build relation elements for the relations component in belief_annotations or sentiment_annotations
    '''
    beliefs = relation_mention.beliefs
    sentiments = relation_mention.sentiments
    #if statement maybe can be ingored if we can produce an relation element without sentiment or beliefs component
    if (is_belief_annotations and beliefs) or (not is_belief_annotations and sentiments):
        ere_id = relation_mention.mention_id
        relation = SubElement(relations, 'relation', ere_id=xstr(ere_id))

        trigger = relation_mention.trigger
        if not trigger is None:
            build_trigger(relation, trigger)

        if is_belief_annotations and beliefs:
            build_beliefs(relation, beliefs)
        elif not is_belief_annotations and sentiments:
            build_sentiments(relation, sentiments)


def build_relations(annotations):
    '''
    Build relations elements for belief_annotations or sentiment_annotations
    '''
    relations = SubElement(annotations, 'relations')
    relation_mentions = best_annotations.ere_annotations.relation_mentions
    for relation_mention in relation_mentions.values():
        if annotations.tag == 'belief_annotations':
            build_relation(relations, relation_mention, True)
        elif annotations.tag == 'sentiment_annotations':
            build_relation(relations, relation_mention, False)


def build_event(events, event_mention, is_belief_annotations):
    '''
    Build event elements for the events component in belief_annotations or sentiment_annotations
    '''
    beliefs = event_mention.beliefs
    sentiments = event_mention.sentiments

    #if statement maybe can be ingored if we can produce an event element without sentiment or beliefs component
    if (is_belief_annotations and beliefs) or (not is_belief_annotations and sentiments):
        ere_id = event_mention.mention_id
        event = SubElement(events, 'event', ere_id=xstr(ere_id))

        trigger = event_mention.trigger
        if not trigger is None:
            build_trigger(event, trigger)

        if is_belief_annotations and beliefs:
            build_beliefs(event, beliefs)

            args = event_mention.arguments.values()
            if args:
                build_arguments(event, args)
        elif not is_belief_annotations and sentiments:
            build_sentiments(event, sentiments)


def build_events(annotations):
    '''
    Build the events element for belief_annotations or sentiment_annotations
    '''
    events = SubElement(annotations, 'events')
    event_mentions = best_annotations.ere_annotations.event_mentions
    for event_mention in event_mentions.values():
        if annotations.tag == 'belief_annotations':
            build_event(events, event_mention, True)
        elif annotations.tag == 'sentiment_annotations':
            build_event(events, event_mention, False)


def build_entities(sentiment_annotations):
    '''
    Build the entities element for sentiment_annotations
    Notice: belief_annotations section does not have the entities element
    '''
    entities = SubElement(sentiment_annotations, 'entities')
    entity_mentions = best_annotations.ere_annotations.entity_mentions
    for entity_mention in entity_mentions.values():
        #here I suppose (from the best.xml) that for each entity, only has one source an atitude on it
        sentiments = entity_mention.sentiments
        if sentiments:
            ere_id = entity_mention.mention_id
            offset = entity_mention.offset
            length = entity_mention.length

            entity = SubElement(entities, 'entity', ere_id=xstr(ere_id), offset=xstr(offset), length=xstr(length))

            entity_text = entity_mention.mention_text
            text_xml = SubElement(entity, 'text')
            text_xml.text = entity_text

            build_sentiments(entity, sentiments)


def build_bestxml(best_annotations):
    '''
    Build and write deft_best.xml file from best_annotations object
    '''
    #Here suppose it just has a single source
    #get the doc_id
    if best_annotations.sources.keys():
        doc_id = best_annotations.sources.keys()[0]

    #build the root:committed_belief_doc
    committed_belief_doc = Element('committed_belief_doc', id=xstr(doc_id))

    #build belief_annotations
    belief_annotations = SubElement(committed_belief_doc, 'belief_annotations')
    build_relations(belief_annotations)
    build_events(belief_annotations)

    #build sentiment_annotations
    sentiment_annotations = SubElement(committed_belief_doc, 'sentiment_annotations')
    build_relations(sentiment_annotations)
    build_events(sentiment_annotations)
    build_entities(sentiment_annotations)
    indent(committed_belief_doc)

    #write to deft_best.xml file
    output_file = open( 'deft_best.xml', 'w' )
    output_file.write(ET.tostring(committed_belief_doc, encoding="UTF-8" ))
    output_file.close()


if __name__ == '__main__':
    if len(sys.argv) == 3:
        ere_annotations = read_ere_xml(sys.argv[1])
        print('Successfully read ERE annotations from {0}.'.format(sys.argv[1]))
        best_annotations = read_best_xml(ere_annotations, sys.argv[2])
        print('Successfully read BeSt annotations from {0}.'.format(sys.argv[2]))
        build_bestxml(best_annotations)
        print('Successfully build the deft_best.xml file from the best_annotations object')
    else:
        print('to_deft_best.py -- library for DEFT ERE and BeSt annotations.')
        print(' This script should terminate without error if the annotations are well formed.')
        print(' Usage: python to_deft_best.py [rich ere XML file] [BeST XML file]')
