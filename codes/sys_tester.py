# -*- coding: utf-8 -*-
'''
Module used to calculate scores for all files (ere.xml, best.xml, predicted.xml)
in a folder, and get averaged results.
'''
import os
import sys
from os import listdir
from os.path import isfile, join
import subprocess

def avg_evaluator(ere_path, gold_path, sys_path):

    test_file_names = [f.split('.')[0] for f in listdir(sys_path) if isfile(join(sys_path, f))]
    p_values = []
    r_values = []
    f_values = []
    for i in xrange(1, len(test_file_names)):
        ere_file_path = join(ere_path, test_file_names[i]) + '.rich_ere.xml'
        gold_best_path = join(gold_path, test_file_names[i]) + '.best.xml'
        sys_best_path = join(sys_path, test_file_names[i]) + '.best.xml'

        if isfile(ere_file_path) and isfile(gold_best_path) and isfile(sys_best_path):
            #get averaged result
            command = ['python', 'best_evaluator.py', '-s', ere_file_path, gold_best_path, sys_best_path]
            proc = subprocess.Popen(command, stdout=subprocess.PIPE, shell=False)
            (out, err) = proc.communicate()

            print 'The scores for the file {0} are (see below): '.format(test_file_names[i])
            print out
            if not out is None and out != '':
                p_value = float(out[out.index('P')+3 : out.index('R')])
                r_value = float(out[out.index('R')+3 : out.index('F')])
                f_value = float(out[out.index('F')+3 : len(out)-1])
                p_values.append(p_value)
                r_values.append(r_value)
                f_values.append(f_value)

    p_avg = sum(p_values) / float(len(p_values))
    r_avg = sum(r_values) / float(len(r_values))
    f_avg = sum(f_values) / float(len(f_values))

    print '\nThe average scores for the system-outputs are: '
    print(' avg P: {0}  avg R: {1}  avg F: {2}'.format(p_avg, r_avg, f_avg))

if __name__ == '__main__':
    if len(sys.argv) == 4:
        ere_eng_path = sys.argv[1]
        best_eng_path = sys.argv[2]
        sys_best_path = sys.argv[3]
        print('Successfully read file pathes')
        avg_evaluator(ere_eng_path, best_eng_path, sys_best_path)
    else:
        print('sys_tester.py -- code to evaluate the average scores of system on BeSt annotations.')
        print(' Usage: python sys_tester.py [path to the folder of ere files] [path to the folder of gold best files] [path to the folder of system prodcued best files]')

    #BE CAUTION: read_best_xml MAY NOT WORK FOR DIR WHERE HAS WHITE SPACE FOR FOLDERS OR FILES
    # ere_eng_path ='/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/all_data/ere'
    # best_eng_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/all_data/annotation'
    # best_eng_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment_20160323_46_src_intra_offset0/annotation'
    # ere_eng_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment/20160331/ere'
    # #Sardar-Belief/xml-belief_annotation
    # # sys_best_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Sardar-Belief/xml-belief_annotation'
    # #Noura-Sentiment/most basic_output/xml-files-new
    # # sys_best_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Noura-Sentiment/most basic_output/xml-files-new'
    # #for all
    # sys_best_path = '/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment/20160331/best_xml'
    # #Marlies-Axinia-Sentiment/20160323
    # sys_best_path = "/Users/taoyds/Documents/Research/Data_DEFT/LDC2016E27_DEFT_English_Belief_and_Sentiment_Annotation/data/system-outputs/Marlies-Axinia-Sentiment/20160323/best_xml"
    # avg_evaluator(ere_eng_path, best_eng_path, sys_best_path)
