import sys
import os
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement
from os import listdir, makedirs
from os.path import isfile, isdir, join, splitext, split, exists

from get_ranges_offsets import get_src_ranges
from add_author_says import transform_pred_best, indent
from add_author_says_newswire import transform_pred_best_nw

def usage():
    print ("python to_pred_best_SINNET.py <input dir>")
    print ("<input dir> is the dir where contains source_original(with The authorsays), best_KB  ere AND ere_original folders.")


def to_pred_best(input_dir):
    """
    transform the predicted best file using 'the author says' approach back to the
    predicted best with origianl offsets, src names and mentiond ids.
    """
    if not isdir(input_dir):
        usage()
    else:
        src_input_dir = os.path.join(input_dir,"source_original")
        ere_input_dir = os.path.join(input_dir,"ere_original")
        best_input_dir = os.path.join(input_dir,"best_KB")
        predicted_best_dir = os.path.join(input_dir,"predicted_best")
        #convert the predicted file names (required for SINNET input) back to original names with '_' or other symbols
        chars_removed = ['.', '_', '-', '-kbp']
        ere_fn_dir = os.path.join(input_dir,"ere")
        original_fns = [f.replace('.rich_ere.xml','') for f in os.listdir(ere_fn_dir) if f.endswith('.rich_ere.xml')]
        fn_mapper = {''.join([c for c in orig_fn if c not in chars_removed]) : orig_fn for orig_fn in original_fns}

        if not exists(predicted_best_dir):
            makedirs(predicted_best_dir)

        filenames = [f for f in os.listdir(src_input_dir) if f.endswith('.cmp.txt')]
        for f in filenames:
            base_name = f.replace('.cmp.txt','')
            print "processing file is: ", base_name
            ere_fn = '{0}.rich_ere.xml'.format(base_name)
            best_fn = '{0}.best.xml'.format(base_name)
            #input files
            src_path = os.path.join(src_input_dir, f)
            ere_path = os.path.join(ere_input_dir, ere_fn)
            best_path = os.path.join(best_input_dir, best_fn)
            #output files
            orig_base_name = fn_mapper[base_name]
            orig_best_fn = '{0}.best.xml'.format(orig_base_name)
            predicted_best_path = os.path.join(predicted_best_dir, orig_best_fn)

            #if ere and best files with the same name as source file exist
            if isfile(ere_path) and isfile(best_path):
                #get new source ranges of authors, posts and quotes
                src_range_new = get_src_ranges(src_path)

                best_tree = ET.parse(best_path)
                best_root = best_tree.getroot()

                ere_tree = ET.parse(ere_path)
                ere_root = ere_tree.getroot()

                #transform the new best files back to original best files as the final predicted best files
                if 'NYT' in base_name or 'APW' in base_name or 'AFP' in base_name:
                    pred_best_root = transform_pred_best_nw(best_root, ere_root, src_range_new)
                else:
                    pred_best_root = transform_pred_best(best_root, ere_root, src_range_new)
                #write predicted best file
                indent(pred_best_root)
                best_new_file = open(predicted_best_path, 'w')
                best_new_file.write(ET.tostring(pred_best_root, encoding="UTF-8" ))
                best_new_file.close()

        print('Successfully write new source, ere and annotation files to {0}.'.format(input_dir))


if __name__ == '__main__':
    if len(sys.argv) == 2:
        to_pred_best(sys.argv[1])
